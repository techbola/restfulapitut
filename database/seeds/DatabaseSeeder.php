<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\User::truncate();
        \App\Category::truncate();
        \App\Product::truncate();
        \App\Transaction::truncate();
        DB::table('category_product')->truncate();

        $usersQauntity = 50;
        $categoriesQauntity = 10;
        $productsQauntity = 30;
        $transactionsQauntity = 30;

        factory(\App\User::class, $usersQauntity)->create();
        factory(\App\Category::class, $categoriesQauntity)->create();

        factory(\App\Product::class, $productsQauntity)->create()->each(
            function ($product){
                $categories = \App\Category::all()->random(mt_rand(1, 5))->pluck('id');
                $product->categories()->attach($categories);
            });

       factory(\App\Transaction::class, $transactionsQauntity)->create();

    }
}
