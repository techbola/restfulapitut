<?php

namespace App;

class Buyer extends User
{

//    a user as a buyer has transactions

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

}
