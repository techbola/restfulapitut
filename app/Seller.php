<?php

namespace App;

class Seller extends User
{

//    a user as a seller has products

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
